def f = namespace(lib.FormTagLib)

f.section(title:_("Pipeline Data Extractor")) {
    f.entry(title:_("Logstash HTTP Input URL"), field:"logstashHttpInputUrl") {
        f.textbox()
    }
    f.entry(title:_("Logstash Monitoring API URL"), field:"logstashMonitoringUrl") {
        f.textbox()
    }
}