package io.jenkins.plugins.pipeline.data.extractor.model;


import org.jenkinsci.plugins.workflow.pipelinegraphanalysis.GenericStatus;

import hudson.model.Result;

public enum Status {
    ABORTED,
    SUCCESS,
    FAILED,
    UNSTABLE,
    IN_PROGRESS,
    NOT_EXECUTED;

    public static Status valueOf(Result r) {
        if (r == Result.NOT_BUILT) {
            return Status.NOT_EXECUTED;
        }
        if (r == Result.ABORTED) {
            return Status.ABORTED;
        }
        if (r == Result.FAILURE) {
            return Status.FAILED;
        }
        if (r == Result.UNSTABLE ) {
            return Status.UNSTABLE;
        }
        if (r == Result.SUCCESS) {
            return Status.SUCCESS;
        } 
        
        throw new IllegalStateException("Illegal Result type: " + r);
        
    }

    public static Status valueOf(GenericStatus st) {
        if (st == null) {
            return Status.NOT_EXECUTED;
        }
        switch (st) {
            case ABORTED: return Status.ABORTED;
            case FAILURE: return Status.FAILED;
            case IN_PROGRESS: return Status.IN_PROGRESS;
            case UNSTABLE: return Status.UNSTABLE;
            case SUCCESS: return Status.SUCCESS;
            case NOT_EXECUTED: return Status.NOT_EXECUTED;
            default:
                throw new IllegalStateException("Illegal GenericStatus: "+st);
        }
    }
}
