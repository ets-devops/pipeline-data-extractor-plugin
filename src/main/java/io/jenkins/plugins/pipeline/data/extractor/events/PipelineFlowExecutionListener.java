package io.jenkins.plugins.pipeline.data.extractor.events;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import org.jenkinsci.plugins.workflow.flow.FlowExecution;
import org.jenkinsci.plugins.workflow.flow.FlowExecutionListener;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import hudson.Extension;
import io.jenkins.plugins.pipeline.data.extractor.annotation.Exclude;
import io.jenkins.plugins.pipeline.data.extractor.model.RunData;
import io.jenkins.plugins.pipeline.data.extractor.service.RunDataService;

@Extension
public class PipelineFlowExecutionListener extends FlowExecutionListener {
    
    private static final Logger LOGGER = Logger.getLogger(PipelineFlowExecutionListener.class.getName());
    private final RunDataService runDataService;

    public PipelineFlowExecutionListener() {
        this.runDataService = new RunDataService();
    }

    @Override
    public void onCompleted(@Nonnull FlowExecution execution) {
        Optional<RunData> optRunData = runDataService.extractRunData(execution);
        RunData runData = optRunData.orElse(null);
        if (runData == null) {
            return;
        }
        runDataService.sendRunData(runData);
        logJson(runData);
    }

    private void logJson(RunData runData) {
        ExclusionStrategy strategy = new ExclusionStrategy() {
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        
            @Override
            public boolean shouldSkipField(FieldAttributes field) {
                return field.getAnnotation(Exclude.class) != null;
            }
        };
        
        Gson gson = new GsonBuilder()
            .setExclusionStrategies(strategy)
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting()
            .create();

        String jsonFormat = gson.toJson(runData);
        LOGGER.log(Level.INFO, jsonFormat);
    }
}