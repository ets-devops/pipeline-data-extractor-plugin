package io.jenkins.plugins.pipeline.data.extractor.service;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jenkinsci.plugins.workflow.flow.FlowExecution;
import org.jenkinsci.plugins.workflow.job.WorkflowRun;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.jenkins.plugins.pipeline.data.extractor.annotation.Exclude;
import io.jenkins.plugins.pipeline.data.extractor.dao.RunDataRepository;
import io.jenkins.plugins.pipeline.data.extractor.dao.RunDataRepositoryHttpImpl;
import io.jenkins.plugins.pipeline.data.extractor.model.RunData;

public class RunDataService {
    private final RunDataRepository runDataRepository;
    private final StageDataService stageDataService;
    private static final Logger LOGGER = Logger.getLogger(RunDataService.class.getName());

    public RunDataService(){
        this.runDataRepository = new RunDataRepositoryHttpImpl();
        this.stageDataService = new StageDataService();
    }

    public Optional<RunData> extractRunData(FlowExecution execution) {
        WorkflowRun run = null;
        try {
            run = (WorkflowRun) execution.getOwner().getExecutable();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        if (run == null) {
            return Optional.empty();
        }
        
        RunData runData= new RunData.RunDataBuilder(execution, run)
            .setStages(stageDataService.initStages(execution, run))
            .create();

        return Optional.of(runData);
    }

    public void sendRunData(RunData runData) {
        if (runData != null) {
            final String jsonPayload = serializeToJson(runData);
            runDataRepository.send(jsonPayload);
        }
    }

    private String serializeToJson(RunData runData) {
        ExclusionStrategy strategy = new ExclusionStrategy() {
            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        
            @Override
            public boolean shouldSkipField(FieldAttributes field) {
                return field.getAnnotation(Exclude.class) != null;
            }
        };
        
        Gson gson = new GsonBuilder()
            .setExclusionStrategies(strategy)
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

        return gson.toJson(runData);
    }
}