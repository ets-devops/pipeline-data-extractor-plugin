package io.jenkins.plugins.pipeline.data.extractor.persistence;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.jenkins.plugins.pipeline.data.extractor.configuration.LogstashConfiguration;

public final class LogstashConnection {

    private static final Logger LOGGER = Logger.getLogger(LogstashConnection.class.getName());

    private final HttpURLConnection connection;

    /**
     * 
     * @throws UnknownHostException
     */
    public LogstashConnection() throws UnknownHostException{  
        final String errorMessage = "Connection to logstash failed";
        if (!checkUrlExists()) {
            throw new UnknownHostException(errorMessage);
        }
        HttpURLConnection con = null;
        try {
            final String LOGSTASH_HTTP_INPUT = LogstashConfiguration.getExtension().getLogstashHttpInputUrl();
            URL url = new URL(LOGSTASH_HTTP_INPUT);
            con = (HttpURLConnection)url.openConnection();
        }
        catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        
        if (con == null) {
            throw new UnknownHostException(errorMessage);
        }
        else{
            this.connection = con;
        }
    }

    private boolean checkUrlExists() {
        final String LOGSTASH_MONITORING_API = LogstashConfiguration.getExtension().getLogstashMonitoringUrl();
        final String LOGSTASH_HTTP_INPUT = LogstashConfiguration.getExtension().getLogstashHttpInputUrl();

        if (LOGSTASH_MONITORING_API == null || LOGSTASH_MONITORING_API.trim().isEmpty() 
            || LOGSTASH_HTTP_INPUT == null || LOGSTASH_HTTP_INPUT.trim().isEmpty()) {
            return false;
        }

        HttpURLConnection con = null;
        try{
            LOGGER.info(LOGSTASH_MONITORING_API);
            URL url = new URL(LOGSTASH_MONITORING_API);
            con = (HttpURLConnection) url.openConnection();
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("HEAD");
            
            int responseCode = con.getResponseCode();
            return responseCode == HttpURLConnection.HTTP_OK;
        }
        catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        finally {
            if (con != null) {
                con.disconnect();
            }
        }

        return false;
    }

    public HttpURLConnection getConnection() {
        return connection;
    }
}