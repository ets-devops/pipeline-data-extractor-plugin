package io.jenkins.plugins.pipeline.data.extractor.model;

public class ErrorData {
    private final String message;
    private final String type;
    
    public ErrorData(String message, String type) {
        this.message = message;
        this.type = type;
    }
    
    public String getMessage() {
        return message;
    }
    public String getType() {
        return type;
    }
}
