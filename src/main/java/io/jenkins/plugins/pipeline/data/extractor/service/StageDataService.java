package io.jenkins.plugins.pipeline.data.extractor.service;

import java.util.ArrayList;
import java.util.List;

import org.jenkinsci.plugins.workflow.flow.FlowExecution;
import org.jenkinsci.plugins.workflow.graph.FlowNode;
import org.jenkinsci.plugins.workflow.graphanalysis.ForkScanner;
import org.jenkinsci.plugins.workflow.job.WorkflowRun;
import org.jenkinsci.plugins.workflow.pipelinegraphanalysis.StageChunkFinder;

import io.jenkins.plugins.pipeline.data.extractor.graph.Visitor;
import io.jenkins.plugins.pipeline.data.extractor.model.StageData;

public class StageDataService {
    
    private final LogDataService logDataService;

    public StageDataService() {
        this.logDataService = new LogDataService();
    }
    public List<StageData> initStages(FlowExecution execution, WorkflowRun run) {
        Visitor visitor = new Visitor(run);
        List<FlowNode> heads = execution.getCurrentHeads();
        if (heads != null) {
            ForkScanner.visitSimpleChunks(heads, visitor, new StageChunkFinder());
        }
        final List<StageData> visitorStages = new ArrayList<>(visitor.getStages());
        visitorStages.sort(StageDataService::compareStageNode);

        return logDataService.initLogs(execution, visitorStages);
    }

    private static int compareStageNode(StageData s1, StageData s2) {
        return Long.compare(s1.getStartTime(), s2.getStartTime());
    }
}