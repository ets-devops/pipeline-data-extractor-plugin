package io.jenkins.plugins.pipeline.data.extractor.events;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import hudson.Extension;
import hudson.model.Queue.LeftItem;
import hudson.model.queue.QueueListener;

@Extension
public class PipelineQueueListener extends QueueListener {

    private static ConcurrentMap<String, Long> waitingTimes;
    private static final int SUBSTRING_BEGIN_INDEX;
    private static final Logger LOGGER;

    static{
        waitingTimes = new ConcurrentHashMap<>(); 
        SUBSTRING_BEGIN_INDEX = 8;
        LOGGER = Logger.getLogger(PipelineQueueListener.class.getName());
    }

    @Override
    public void onLeft(LeftItem li) {
        final String taskFullName = li.task.getFullDisplayName();
        if (taskFullName.length()<SUBSTRING_BEGIN_INDEX) {
            return; // not part of the build
        }

        final long inQueueSince = li.getInQueueSince();
        final Long queueDuration = System.currentTimeMillis() - inQueueSince;
        final String fullName = taskFullName.substring(SUBSTRING_BEGIN_INDEX);
        
        final Long previous = waitingTimes.put(fullName, queueDuration);
        log(taskFullName, queueDuration);

        if (previous != null) {
            waitingTimes.put(fullName, previous+queueDuration);
        }
    }

    public static long getQueueDuration(String fullName){
        return waitingTimes.remove(fullName);
    }

    private void log(String taskFullName, long queueDuration) {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO,String.format("The task \"%s\" has left the queue with a wating time of: %d milliseconds", taskFullName, queueDuration));  
        }
    }
}