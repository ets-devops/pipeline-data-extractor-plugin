package io.jenkins.plugins.pipeline.data.extractor.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import io.jenkins.plugins.pipeline.data.extractor.persistence.LogstashConnection;

public class RunDataRepositoryHttpImpl implements RunDataRepository {

    private static final Logger LOGGER = Logger.getLogger(RunDataRepositoryHttpImpl.class.getName());

    @Override
    public Optional<String> send(String data) {
        if (data == null) {
            return Optional.empty();
        }

        HttpURLConnection con = null;
        try {
            LogstashConnection logstashConnection = new LogstashConnection();
            con = logstashConnection.getConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            if(con != null){
                con.disconnect();
            }
        }
        if (con == null) {
            return Optional.empty();
        }

        try (OutputStream os = con.getOutputStream()) {
            byte[] input = data.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

        //Read the response
        try (BufferedReader br = new BufferedReader(
            new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
 
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            String jsonResponse = new Gson().toJson(response.toString());
            LOGGER.info(jsonResponse);
            return Optional.ofNullable(jsonResponse);
            
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

        return Optional.empty();
    }
    
}