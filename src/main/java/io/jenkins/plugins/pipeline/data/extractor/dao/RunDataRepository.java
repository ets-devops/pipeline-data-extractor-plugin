package io.jenkins.plugins.pipeline.data.extractor.dao;

import java.util.Optional;

public interface RunDataRepository {
    Optional<String> send(String jsonPayload);
}