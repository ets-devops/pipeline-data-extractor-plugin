package io.jenkins.plugins.pipeline.data.extractor.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jenkinsci.plugins.workflow.flow.FlowExecution;
import org.jenkinsci.plugins.workflow.graph.FlowNode;
import org.jenkinsci.plugins.workflow.graphanalysis.ForkScanner;

import io.jenkins.plugins.pipeline.data.extractor.model.LogData;
import io.jenkins.plugins.pipeline.data.extractor.model.StageData;

public class LogDataService {

    public List<StageData> initLogs(FlowExecution execution, List<StageData> stages) {
        ForkScanner forkScanner = new ForkScanner();
        List<FlowNode> flowNodes = forkScanner.allNodes(execution);
        Map<FlowNode,List<FlowNode>> nodesParents= new HashMap<>();
        for (FlowNode flowNode : flowNodes) {
            if (LogData.hasLogs(flowNode)) {
                nodesParents.put(flowNode, flowNode.getParents());
            }
        }
        
        nodesParents.forEach((node,parents) -> {
            for (StageData stage : stages) {
                for (FlowNode parent : parents) {
                    if(parent.getId().equals(stage.getId())){
                        stage.setLogs(new LogData(node));
                        break;
                    }
                }
            }
        });
        return stages;
    }
}