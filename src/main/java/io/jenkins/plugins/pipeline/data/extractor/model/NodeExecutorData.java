package io.jenkins.plugins.pipeline.data.extractor.model;

public class NodeExecutorData {
    private final String computerName;
    private final String executorName;
    private final int executorNumber;

    public NodeExecutorData(String computerName, String executorName, int executorNumber) {
        this.computerName = computerName;
        this.executorName = executorName;
        this.executorNumber = executorNumber;
    }

    public String getComputerName() {
        return computerName;
    }
    public String getExecutorName() {
        return executorName;
    } 
    public int getExecutorNumber() {
        return executorNumber;
    }
}