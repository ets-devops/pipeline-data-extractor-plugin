package io.jenkins.plugins.pipeline.data.extractor.model;

import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jenkinsci.plugins.workflow.actions.LogAction;
import org.jenkinsci.plugins.workflow.graph.FlowNode;

import hudson.console.AnnotatedLargeText;

public class LogData {
    private String text;
    private long length;

    private static final int START_INDEX;
    private static final Logger LOGGER;

    static{
        LOGGER = Logger.getLogger(LogData.class.getName());
        START_INDEX = 0;
    }

    public LogData(FlowNode node) {
        LogAction logAction = node.getAction(LogAction.class);
        if (logAction == null) {
            return;
        }
        AnnotatedLargeText<? extends FlowNode> logText = logAction.getLogText();
        if (logText == null) {
            return;
        }
        long logLength = logText.length();
        if (logLength <= START_INDEX) {
            return;
        }
        this.length = logLength;
        StringWriter writer = new StringWriter();
        try {
            logText.writeHtmlTo(START_INDEX, writer);
            this.text = writer.toString();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error serializing log for", e);
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public static boolean hasLogs(FlowNode node) {
        if (node == null) {
            return false;
        }
        LogAction logAction = node.getAction(LogAction.class);
        if (logAction == null) {
            return false;
        }
        AnnotatedLargeText<? extends FlowNode> logText = logAction.getLogText();
        if (logText == null) {
            return false;
        }
        long logLength = logText.length();
        return logLength > START_INDEX;
    }
}
