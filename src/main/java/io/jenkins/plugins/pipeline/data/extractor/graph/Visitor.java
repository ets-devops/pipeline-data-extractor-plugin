package io.jenkins.plugins.pipeline.data.extractor.graph;

import java.util.ArrayDeque;
import java.util.Deque;

import javax.annotation.Nonnull;

import org.jenkinsci.plugins.workflow.actions.ArgumentsAction;
import org.jenkinsci.plugins.workflow.actions.ErrorAction;
import org.jenkinsci.plugins.workflow.actions.NotExecutedNodeAction;
import org.jenkinsci.plugins.workflow.actions.TimingAction;
import org.jenkinsci.plugins.workflow.graph.FlowNode;
import org.jenkinsci.plugins.workflow.graphanalysis.ForkScanner;
import org.jenkinsci.plugins.workflow.graphanalysis.MemoryFlowChunk;
import org.jenkinsci.plugins.workflow.graphanalysis.StandardChunkVisitor;
import org.jenkinsci.plugins.workflow.job.WorkflowRun;
import org.jenkinsci.plugins.workflow.pipelinegraphanalysis.GenericStatus;
import org.jenkinsci.plugins.workflow.pipelinegraphanalysis.StatusAndTiming;
import org.jenkinsci.plugins.workflow.pipelinegraphanalysis.TimingInfo;

import hudson.model.Executor;
import io.jenkins.plugins.pipeline.data.extractor.model.ErrorData;
import io.jenkins.plugins.pipeline.data.extractor.model.NodeExecutorData;
import io.jenkins.plugins.pipeline.data.extractor.model.StageData;
import io.jenkins.plugins.pipeline.data.extractor.model.Status;

public class Visitor extends StandardChunkVisitor {

    private Deque<StageData> stages;
    private final WorkflowRun run;
    private FlowNode firstExecuted;

    /**
     * 
     * @param run
     * @throws IllegalArgumentException run cannot be null
     */
    public Visitor(WorkflowRun run) {
        if (run == null) {
            throw new IllegalArgumentException("Invalid argument : run cannot be null");
        }
        this.run = run;
        this.stages = new ArrayDeque<>();
    }
    
    @Override
    protected void handleChunkDone(@Nonnull MemoryFlowChunk chunk) {
        final FlowNode chunkFirstNode = chunk.getFirstNode();
        StageData stageData = new StageData();

        stageData.setId(chunkFirstNode.getId());
        stageData.setName(chunkFirstNode.getDisplayName());
        stageData.setParameterDescription(ArgumentsAction.getStepArgumentsAsString(chunkFirstNode));

        Executor executor = run.getExecutor();
        if (executor != null) {
            final String computerName = executor.getOwner().getDisplayName();
            final String executorName = executor.getDisplayName();
            final int executorNumber = executor.getNumber();
            stageData.setExecutor(new NodeExecutorData(computerName, executorName, executorNumber));
        }

        TimingInfo timingInfo;
        long startTime = 0;
        GenericStatus status;

        if (firstExecuted != null) {
            FlowNode last = (chunk.getLastNode() != null) ? chunk.getLastNode() : chunk.getFirstNode(); // Extra safety measure for accidental nesting
            timingInfo = StatusAndTiming.computeChunkTiming(run, chunk.getPauseTimeMillis(), firstExecuted, last, chunk.getNodeAfter());
            startTime = TimingAction.getStartTime(firstExecuted);
            status = StatusAndTiming.computeChunkStatus2(run, chunk.getNodeBefore(), firstExecuted, last, chunk.getNodeAfter());
        }
        else { 
            timingInfo = new TimingInfo(0, 0, run.getStartTimeInMillis());
            status = GenericStatus.NOT_EXECUTED;
        }

        stageData.setStartTime(startTime);
        stageData.setStatus(Status.valueOf(status));

        if (timingInfo != null) {
            stageData.setDuration(timingInfo.getTotalDurationMillis());
        }
        else {
            stageData.setDuration(0);
        }

        ErrorAction errorAction = chunk.getLastNode().getError();
        if (status != GenericStatus.NOT_EXECUTED &&  errorAction != null) {
            Throwable t = errorAction.getError();
            stageData.setError(new ErrorData(t.getMessage(), t.getClass().getName()));
        }

        stages.push(stageData);
    }

    @Override
    public void chunkStart(@Nonnull FlowNode startNode, FlowNode beforeBlock, @Nonnull ForkScanner scanner) {
        if (NotExecutedNodeAction.isExecuted(startNode)) {
            firstExecuted = startNode;
        }
        super.chunkStart(startNode, beforeBlock, scanner);
    }

    public Deque<StageData> getStages() {
        return stages;
    }

    @Override
    public void chunkEnd(@Nonnull FlowNode endNode, FlowNode afterChunk, @Nonnull ForkScanner scanner) {
        super.chunkEnd(endNode, afterChunk, scanner);
        firstExecuted = null;
    }
}
