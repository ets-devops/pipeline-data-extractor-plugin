package io.jenkins.plugins.pipeline.data.extractor.configuration;

import org.jenkinsci.Symbol;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import hudson.Extension;
import hudson.util.FormValidation;
import jenkins.model.GlobalConfiguration;
import net.sf.json.JSONObject;

@Extension
@Symbol("logstash")
public class LogstashConfiguration extends GlobalConfiguration {
    private String logstashHttpInputUrl;
    private String logstashMonitoringUrl;
    
    public LogstashConfiguration() {
        load();
    }

    @Override
    public boolean configure(StaplerRequest req, JSONObject json) throws FormException {
        req.bindJSON(this, json);
        save();
        return true;
    }

    public FormValidation doCheckLogstashHttpInputUrl(@QueryParameter("logstashHttpInputUrl") String url) {
        return validateUrl(url);
    }

    public FormValidation doCheckLogstashMonitoringUrl(@QueryParameter("logstashMonitoringUrl") String url) {
        return validateUrl(url);
    }
    
    private FormValidation validateUrl(String url) {
        if (url == null || url.trim().isEmpty()) {
            return FormValidation.error("Specify a valid URL (http://host:port/url-path)");
        }
        if (!isProtocolValid(url)){
            return FormValidation.error("Invalid protocol. Only http is supported.");
        }
        return FormValidation.ok();
    }

    private boolean isProtocolValid(String url) {
        return url.startsWith("http://") || url.startsWith("https://");
    }

    public static LogstashConfiguration getExtension() {
        return GlobalConfiguration.all().get(LogstashConfiguration.class);
    }
    
    public String getLogstashHttpInputUrl() {
        return logstashHttpInputUrl;
    }
    public void setLogstashHttpInputUrl(String url) {
        logstashHttpInputUrl = url;
    }

    public String getLogstashMonitoringUrl() {
        return logstashMonitoringUrl;
    }
    public void setLogstashMonitoringUrl(String url) {
        logstashMonitoringUrl = url;
    }
}
