package io.jenkins.plugins.pipeline.data.extractor.model;

public class StageData {
    private String id;
    private String name;
    private Status status;
    private long startTime;
    private long duration;
    private String parameterDescription;
    private NodeExecutorData executor;
    private ErrorData error;
    private LogData logs;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public ErrorData getError() {
        return error;
    }
    public void setError(ErrorData error) {
        this.error = error;
    }
    public String getParameterDescription() {
        return parameterDescription;
    }
    public void setParameterDescription(String parameterDescription) {
        this.parameterDescription = parameterDescription;
    }
    public long getStartTime() {
        return startTime;
    }
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    public long getDuration() {
        return duration;
    }
    public void setDuration(long duration) {
        this.duration = duration;
    }
    public NodeExecutorData getExecutor() {
        return executor;
    }
    public void setExecutor(NodeExecutorData executor) {
        this.executor = executor;
    }
    public LogData getLogs() {
        return logs;
    }
    public void setLogs(LogData logs) {
        this.logs = logs;
    }
}
