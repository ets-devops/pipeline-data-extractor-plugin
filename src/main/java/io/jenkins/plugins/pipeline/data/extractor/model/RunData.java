package io.jenkins.plugins.pipeline.data.extractor.model;

import java.util.List;

import org.jenkinsci.plugins.workflow.flow.FlowExecution;
import org.jenkinsci.plugins.workflow.job.WorkflowRun;

import hudson.model.Result;
import io.jenkins.plugins.pipeline.data.extractor.events.PipelineQueueListener;

/**
 * Aggregates the run metadata and the logs for this build and all its stages 
 */
public class RunData {
    private final String id; // identical to number for builds created in Jenkins versions 1.597+
    private final int number;
    private final String jobName; // The job this build is for
    private final String fullName;
    private final Status status;
    private final long startTime;
    private final long endTime;
    private final long duration;
    private final long queueDuration;
    private String errorMessage;
    private final List<StageData> stages;
    
    public RunData(RunDataBuilder runDataBuilder) {
        this.id = runDataBuilder.id;
        this.number = runDataBuilder.number;
        this.jobName = runDataBuilder.jobName; 
        this.fullName = runDataBuilder.fullName;
        this.status = runDataBuilder.status;
        this.startTime = runDataBuilder.startTime;
        this.duration = runDataBuilder.duration;
        this.queueDuration = runDataBuilder.queueDuration;
        this.errorMessage = runDataBuilder.errorMessage;  
        this.endTime = runDataBuilder.endTime;
        this.stages = runDataBuilder.stages;
    }

    public static class RunDataBuilder {
        private final String id; // identical to number for builds created in Jenkins versions 1.597+
        private final int number;
        private final String jobName; // The job this build is for
        private final String fullName;
        private final Status status;
        private final long startTime;
        private final long endTime;
        private final long duration;
        private final long queueDuration;
        private String errorMessage;
        private List<StageData> stages;

        private final FlowExecution execution;
        private final WorkflowRun run;

        /**
         * Creates a RunDataBuilder instance that can be used to build RunData with optional Stages data.
         * RunDataBuilder follows the builder pattern, and it is typically used by first invoking setters
         * for optional class members, and finally calling create()
         * @param execution
         * @param run
         * @throws IllegalArgumentException if {@code execution} or {@code run} are {@value null} or if they are not attached to the same build
         * 
         */
        public RunDataBuilder(FlowExecution execution, WorkflowRun run) {
            if (run == null || execution == null || !isSameBuild(execution, run)) {
                throw new IllegalArgumentException("FlowExecution and WorkflowRun must be non null for RunData and attached to the same build");
            }
            this.execution = execution;
            this.run = run;

            this.id = run.getId();
            this.number = run.getNumber();
            this.jobName = run.getParent().getFullDisplayName(); 
            this.fullName = run.getFullDisplayName();
            this.startTime = run.getStartTimeInMillis();
            this.duration = run.getDuration();
            this.queueDuration = PipelineQueueListener.getQueueDuration(fullName);

            final Throwable causeOfFailure = execution.getCauseOfFailure();
            if (causeOfFailure != null) {
                this.errorMessage = causeOfFailure.getMessage();  
            }

            this.status = initStatus();
            if (status == Status.IN_PROGRESS) {
                this.endTime = System.currentTimeMillis();
            }
            else{
                this.endTime = startTime + duration;
            }
        }

        private boolean isSameBuild(FlowExecution execution, WorkflowRun run) {
            final FlowExecution execution2 = run.getExecution();
            if (execution2 == null) {
                return false;
            }
            return execution2.equals(execution);
        }

        private Status initStatus() {
            if (execution == null) {
                return Status.NOT_EXECUTED;
            }
            if (execution.getCauseOfFailure() != null) {
                return Status.FAILED;
            }
            if (execution.isComplete()) {
                Result result = run.getResult();
                return Status.valueOf(result);
            }
            return Status.IN_PROGRESS;
        }

        public RunDataBuilder setStages(List<StageData> stages) {
            this.stages = stages;
            return this;
        }

        public RunData create() {
            return new RunData(this);
        }
    }

    public String getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public String getJobName() {
        return jobName;
    }

    public String getFullName() {
        return fullName;
    }

    public Status getStatus() {
        return status;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getDuration() {
        return duration;
    }

    public long getQueueDuration() {
        return queueDuration;
    }

    public List<StageData> getStages() {
        return stages;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}